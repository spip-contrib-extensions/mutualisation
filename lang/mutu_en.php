<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mutu?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'install_bd_cree' => '@nombase@ database has been created',
	'install_bd_echec' => 'Creation of @nombase@ database failed.',
	'install_bd_reessayer' => 'Try again...',
	'install_code' => 'Please, enter the activation code:',
	'install_code_panel' => 'Please type your usual password or the activation code provided by the hosting service:',
	'install_creation_bd' => 'Creation of @nombase@ database',
	'install_creation_bd_site' => 'Creation of the database for the website ',
	'install_creation_bd_site_2' => 'The database @bqse@ for the website @site@ has been created.',
	'install_creation_rep_ok_1' => 'Directories’ creation OK. You can ',
	'install_creation_rep_ok_2' => 'continue...',
	'install_creation_repertoire' => 'Website’s directory creation (@repertoire@)',
	'install_creation_site' => 'Website @site@ creation',
	'install_creer_bd_1' => 'Do you want to ',
	'install_creer_bd_2' => 'create this database?',
	'install_creer_rep_1' => 'Do you want to ',
	'install_creer_rep_2' => 'create the directories for this website?',
	'install_err' => 'Error...',
	'install_no_data_connexion' => 'Connection data @connexion@ are not defined, it’s impossible to automatically create the database.',
	'install_rep_bd_ok' => 'The directory and the database for the website are now created.',
	'install_repertoire_creer' => 'Please create the directory @repertoire@ and its sub-directories:',
	'install_repertoire_inaccessible' => 'Can’t write in directory @repertoire@.',
	'install_repertoire_noexist' => 'The website’s directory (@repertoire@) doesn’t exist.',
	'install_reps_crees' => 'The website @site@ directories have been created.',
	'install_site' => 'Installation of your SPIP website',
	'install_spip_1' => 'You can ',
	'install_spip_2' => 'continue with SPIP installation',
	'install_spip_3' => 'You can <a href="@url@">continue with SPIP installation</a>.',

	// M
	'message_site_desactive' => 'Please contact the <a href="@lien@">hosting service</a> for more details',

	// S
	'site_non_active' => 'This site has not yet been validated by an administrator.',
	'site_non_demande' => 'This website doesn’t exist.',
	'site_supprime' => 'This site is awaiting definitive deletion.',
	'site_suspendu' => 'This site is suspended.'
);
