<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mutu?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'install_bd_cree' => 'La base de datos @nombase@ fue creada correctamente.',
	'install_bd_echec' => 'La creación de la base de datos @nombase@ fracasó.',
	'install_bd_reessayer' => 'Volver a intentar',
	'install_code' => 'Favor ingresar el código de activación del sitio:',
	'install_code_panel' => 'Introduzca por favor su contraseña habitual o el código de activación proporcionado por el alojamiento:',
	'install_creation_bd' => 'Creación de la base de datos @nombase@',
	'install_creation_bd_site' => 'Creación de la base de datos del sitio ',
	'install_creation_bd_site_2' => 'La base de datos @base@ fue creada para el sitio @site@.',
	'install_creation_rep_ok_1' => 'Creación de las carpetas OK. Puedes ',
	'install_creation_rep_ok_2' => 'continuar...',
	'install_creation_repertoire' => 'Creación de la carpeta del sitio (@repertoire@)',
	'install_creation_site' => 'Creación del sitio @site@',
	'install_creer_bd_1' => '¿Quieres ',
	'install_creer_bd_2' => 'crear esta base?',
	'install_creer_rep_1' => '¿Quieres ',
	'install_creer_rep_2' => 'crear las carpetas de este sitio?',
	'install_err' => 'Error...',
	'install_no_data_connexion' => 'Los datos de conexión @connexion@ no están definidos, imposible crear automáticamente la base.',
	'install_rep_bd_ok' => 'Las carpetas y la base de datos del sitio ya están creadas.',
	'install_repertoire_creer' => 'Favor crear la carpeta @repertoire@ y sus sub-carpetas:',
	'install_repertoire_inaccessible' => 'No se tienen derechos de escritura en la carpeta @repertoire@',
	'install_repertoire_noexist' => 'La carpeta del sitio (@repertoire@) no existe',
	'install_reps_crees' => 'Las carpetas del sitio @site@ fueron creadas.',
	'install_site' => 'Instalación de tu sitio SPIP',
	'install_spip_1' => 'Puedes ',
	'install_spip_2' => 'proseguir con la instalación de SPIP',
	'install_spip_3' => 'Puede <a href="@url@">continuar la instalación de SPIP</a>.',

	// M
	'message_site_desactive' => 'Contacte por favor el<a href="@lien@">alojamiento</a> para más detalles',

	// S
	'site_non_active' => 'Este sitio aún no ha sido validado por un administrador.',
	'site_non_demande' => 'Este sitio ya no existe.',
	'site_supprime' => 'Este sitio está a la espera de eliminación definitiva.',
	'site_suspendu' => 'Este sitio está suspendido.'
);
