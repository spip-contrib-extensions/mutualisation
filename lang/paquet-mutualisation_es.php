<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mutualisation?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mutualisation_description' => 'Este plugin contiene algunas funciones que facilitan la mutualización de sitios, en particular: <code>demarrer_site()</code>, <code>prefixe_mutualisation()</code> y <code>mutualiser_creer()</code>.  Cuidado, este plugin no se instala como los demás (ver documentación).',
	'mutualisation_nom' => 'Mutualización fácil',
	'mutualisation_slogan' => 'Crear una granja en SPIP'
);
