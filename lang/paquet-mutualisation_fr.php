<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/mutualisation.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mutualisation_description' => 'Ce plugin contient quelques fonctions facilitant la mutualisation de sites : notamment <code>demarrer_site()</code>, <code>prefixe_mutualisation()</code> et <code>mutualiser_creer()</code>.
		Attention ce plugin ne s’installe pas comme les autres (cf. documentation).',
	'mutualisation_nom' => 'Mutualisation facile',
	'mutualisation_slogan' => 'Créer une ferme à SPIP'
);
